from django.views.generic import TemplateView, RedirectView
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import render, redirect, get_object_or_404
from demographics.forms import DemographicsForm
from demographics.models import Demographics
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


class DemoView(TemplateView):
    template_name = 'demographics/demographics.html'

    def get(self, request):
        form = DemographicsForm()

        # if you want to display on the demographics objects
        # belonging to a specific user, this is done by adding a filter
        #
        demos = Demographics.objects.filter(user = request.user)

        context = {
            'demos': demos, 'form': form
        }

        return render(request, self.template_name, context)





    #def get_queryset(self):
        #print(self.request.user)
        #return Demographics.objects.filter(user=self.request.user)


class DemoCreate(CreateView):
    model = Demographics
    fields = ['first_name'] #'middle_name','last_name']

    def form_valid(self, form):
        '''
        How to create a form and add the user instance as a foreign key is
        described at 
        https://docs.djangoproject.com/en/1.11/topics/class-based-views/generic-editing/#models-and-request-user
        '''
        form.instance.user = self.request.user


        return super(DemoCreate, self).form_valid(form)


class EditDemographicsView(UpdateView):  #Note that we are using UpdateView and not FormView
    model = Demographics
    form_class = DemographicsForm
    template_name = 'demographics/demographics.html'

    u = Demographics.objects.get(id=51)

    u.first_name = 'brad'
    u.save()

    def get_object(self, *args, **kwargs):
        user = get_object_or_404(User, pk=self.kwargs['pk'])

        # We can also get user object using self.request.user  but that doesnt work
        # for other models.
# this is new version
        return user.demographics

    def get_success_url(self, *args, **kwargs):
        return reverse('demographics:demographics')


    



