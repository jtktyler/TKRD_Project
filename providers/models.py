from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

class Provider(models.Model):
     pc_doc = models.CharField(max_length=50, null=True)
     pc_practice = models.CharField(max_length=50, null=True)
     pc_number = models.CharField(max_length=50, null=True)
     user = models.ForeignKey(User, null=True)

class Specialist(models.Model):
     spec_doc = models.CharField(max_length=50, null=True)
     spec_practice = models.CharField(max_length=50, null=True)
     spec_number = models.CharField(max_length=50, null=True)
     user = models.ForeignKey(User, null=True)

class Hospital(models.Model):
     hospital_name = models.CharField(max_length=50, null=True)
     hospital_number = models.CharField(max_length=50, null=True)

class Pharmacy(models.Model):
     pharmacy_name = models.CharField(max_length=50, null=True)
     pharmacy_number = models.CharField(max_length=50, null=True)

