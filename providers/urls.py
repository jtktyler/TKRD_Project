from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^providers/$', views.providers, name='providers'),
    url(r'^providers_form/$', views.providers_form, name='providers_form'),

]
