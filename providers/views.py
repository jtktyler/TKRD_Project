from django.shortcuts import render


def providers(request):
    return render(request, 'providers/providers.html')


def providers_form(request):
    return render(request, 'providers/providers_form.html')