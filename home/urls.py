from django.conf.urls import url
from home.views import HomeView
from . import views

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^solutions/$', views.solutions, name='solutions'),
    url(r'^FAQ/$', views.FAQ, name='FAQ'),
    url(r'^about_us/$', views.about_us, name='about_us'),
]
