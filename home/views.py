from django.views.generic import TemplateView
from django.shortcuts import render

class HomeView(TemplateView):
    template_name = 'home/home.html'



def solutions(request):
    return render(request, 'home/solutions.html')

def about_us(request):
    return render(request, 'home/about-us.html')

def FAQ(request):
    return render(request, 'home/FAQ.html')