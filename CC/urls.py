"""CC URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from CC import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    
    url(r'^$', views.login_redirect, name='login_redirect'),
    url(r'^admin/', admin.site.urls),
    url(r'^account/', include('account.urls', namespace='account')),
    url(r'^demographics/', include('demographics.urls', namespace='demographics')),
    url(r'^drug_allergies/', include('drug_allergies.urls', namespace='drug_allergies')),
    url(r'^family_history/', include('family_history.urls', namespace='family_history')),
    url(r'^file_upload/', include('file_upload.urls', namespace='file_upload')),
    url(r'^home/', include('home.urls', namespace='home')),
    url(r'^medical_history/', include('medical_history.urls', namespace='medical_history')),
    url(r'^medications/', include('medications.urls', namespace='medications')),
    url(r'^providers/', include('providers.urls', namespace='providers')),
    url(r'^surgical_history/', include('surgical_history.urls', namespace='surgical_history')),
    url(r'^symptom_tracker/', include('symptom_tracker.urls', namespace='symptom_tracker')),






] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
