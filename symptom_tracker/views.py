from django.views.generic import TemplateView
from django.shortcuts import render

class Symptom_TrackerView(TemplateView):
    template_name = 'home/home.html'
