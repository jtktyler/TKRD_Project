from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import render
from demographics.forms import DemographicsForm
from demographics.models import Demographics



class DemoView(TemplateView):
    template_name = 'demographics/demographics.html'

    def get(self, request):
        form = DemographicsForm()

        # if you want to display on the demographics objects
        # belonging to a specific user, this is done by adding a filter
        #
        demos = Demographics.objects.filter(user = request.user)

        context = {
            'demos': demos, 'form': form
        }

        return render(request, self.template_name, context)


class DemoCreate(CreateView):
    model = Demographics
    fields = ['first_name', 'middle_name','last_name']

    def form_valid(self, form):
        '''
        How to create a form and add the user instance as a foreign key is
        described at 
        https://docs.djangoproject.com/en/1.11/topics/class-based-views/generic-editing/#models-and-request-user
        '''
        form.instance.user = self.request.user
        return super(DemoCreate, self).form_valid(form)


class DemoUpdateView(UpdateView):
    model = Demographics
    fields = ['first_name', 'middle_name','last_name']

    def get_queryset(self):
        qs = super(DemoUpdateView, self).get_queryset()

        return qs.filter(user=self.request.user)

