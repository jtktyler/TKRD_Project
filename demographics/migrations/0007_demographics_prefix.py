# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-23 23:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('demographics', '0006_demographics_middle_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='demographics',
            name='prefix',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
