# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from demographics.models import Demographics

class DemographicsAdmin(admin.ModelAdmin):

    list_display = ('user', 'first_name', 'middle_name', 'last_name')

admin.site.register(Demographics, DemographicsAdmin)

