from django import forms
from .models import Demographics
from django.contrib.auth.forms import UserChangeForm


class DemographicsForm(forms.ModelForm):
    class Meta:
        model = Demographics
        fields = ('first_name', 'last_name', 'middle_name')


class DemoEditForm(forms.ModelForm):
    class Meta:
        model = Demographics
        fields = ('first_name', 'last_name')