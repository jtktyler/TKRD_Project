from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.DemoView.as_view(), name='demographics'),
    url(r'^form/$', views.DemoCreate.as_view(), name='create_demographics'),
    url(r'edit/(?P<pk>\d+)/$', views.DemoUpdateView.as_view(), name='demo_update'),

]

