# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

# Create your models here.

'''
Observation:
    the django.contrib.auth.User model is a foreign key to the Demographics
    model. The User model has it's own first_name and last_name fields. As
    such if we were to have them repeated here, that leads to redundancy
    and possibly conflicting data. For example if the user has the first and
    last names in the User field and they differ from what we have here, 
    which one is the right one?

    The solution, delete the first_name and last_name fields

    Oh perhaps is this supposed to be the 'User' recording data about other people?
    In which case this model would be correct.
'''

class Demographics(models.Model):
     first_name = models.CharField(max_length=50, null=True)
     middle_name = models.CharField(max_length=50, null=True)
     last_name = models.CharField(max_length=50, null=True)
     prefix = models.CharField(max_length=50, null=True)
     user = models.ForeignKey(User, null=True)
     
     def __str__(self):
          return self.user.first_name + ' - ' + self.middle_name 

     def get_absolute_url(self):
          return reverse('demographics:demographics')

